# Remote Portable Software Resource Kit

## A short description
Portable Software generates non-contained resources.
To ensure the resources of said portable software containing private information are not left behind, I've developed a script which cleans locally, pulls from and syncs to a remote server.
With one client script and 3 server-side scripts containing a multitude of functions, I've created a workflow that ensures the portable-resources for my portable software are always accessible.
This was pretty much hacked together out of necessity at first and has been refined over the recent past.
While there is plenty of syncing software out there, I wanted to do something by myself, as a PoC.

Currently the 3 server-side scripts are geared towards Nextcloud instances, particularly docker containers with Nextcloudpi - But this can be easily changed due to the level of modularity - And is accessed via WebDAV over HTTPS.
The client-side, for the time being, is Windows only, but a Linux version is planned at some point. It also requires the portable 7zip software to be present.

The client-side has a simple interactive menu for downloading, uploading and cleaning the local resources.
Server-side, namely your web-server, should consist of 3 branches - Triage, Latest, and Backup. These are explained below.

## Special Thanks
Special thanks to the developers of the included dependancies. 
This workflow merely chains their extensive work.

## Example Workflow
1. Client-script option "Upload" is chosen. **<--- This is the only Human input required.**
2. Client-script compresses the Resources folder into a .7z utilising the 7zip cli software.
3. Client-script uploads data to remote server Triage folder.
4. Client-script cleans all local resources.
5. First server-script ensures Host & Web services remain up.
6. First server-script regularly scans for new content in Triage folder(timer can be changed)
7. First server-script detects new content, calls second-script.
8. Second server-script Checks for current content in Latest.
    8.1. If True, moves that content to backup branch.
        8.1.1. Moves Triage content to Latest.
    8.2. Else, Moves Triage content to Latest.
9. Second server-script calls third-script
10. Third server-script runs an ncp-scan, chown + chmod to ensure the new content is detected(this is fully optional based on the type of webserver you run).
11. Client-script option "Download" is chosen. **<--- Rounding out the workflow.**
12. Client-script downloads .7z from Latest branch.
13. Client-script extracts .7z contents utilising the 7zip cli software.
14. Client-script cleans .7z archives.

As an example, I have used this for over a year for software such as Atom(Project directory), Telegram(tdata) and Joplin(JoplinProfile).
By creating a folder structure identical between the different computers I work on, all this becomes seamless.

## Notes
- On setup, all scripts will require some manual input, as you'll need to point it to your server, amongst other assets.
- All scripts are intentionally somewhat verbose in order to communicate the workflow.

