import subprocess
import os
import sys
import platform
from colorama import init, Fore
import base64
from webdav3.client import Client, WebDavException
import shutil
# Improvements can be done to shave-off some dependencies.

def syscheck():
	global ServerStatus, sys

	sys = Fore.BLUE + platform.system()

	target = '' # Target Server

	Servercheck = ("ping -n 2 {}" .format(target))

	try:
		subprocess.check_call(Servercheck, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)

		ServerStatus = True

	except subprocess.CalledProcessError:
		ServerStatus = False


def menu():
	global ServerStatus, sys, path

	print(Fore.YELLOW + "############################################################################")
	if ServerStatus:
		ServerStatus = (Fore.GREEN + "Online")
		print(Fore.GREEN + "## Server Status: {}" .format(ServerStatus))

	else:
		ServerStatus = Fore.RED + "Offline"
		print(Fore.RED + "## Server Status: {}" .format(ServerStatus))

	print(Fore.BLUE + "## Local System: {}" .format(sys))

	print(Fore.RED + "## THIS SCRIPT SHOULD BE PLACED IN THE PARENT DIRECTORY OF DEPLOYMENT")
	print(Fore.BLUE + "cwd: {}" .format(path))
	print(Fore.YELLOW + "##############################################")
	print(Fore.YELLOW + "## Please choose one of the following options:")
	print(Fore.YELLOW + "##")
	print(Fore.YELLOW + "## 1) Download")
	print(Fore.YELLOW + "## 2) Upload")
	print(Fore.YELLOW + "## 3) Clean-up")
	print(Fore.YELLOW + "## 4) Exit")
	print(Fore.YELLOW + "##")
	print(Fore.YELLOW + "############################################################################")

	Option = input(">>>> ")
	if Option == '1':
		print("Download!")
		SyncBits.Download()
	elif Option == '2':
		print("Upload!")
		SyncBits.Upload()
	elif Option == '3':
		print("Clean-uP")
		SyncBits.CleanUp()
	elif Option == '4':
		exit()
	else:
		exit()


class SyncBits(object):

	def CryptSyncVars(): # Using Base64 so it is not clear-text. Use HTTPS for over-the-wire transfers.
		a1x = base64.b64decode('d2ViZGF2X2hvc3RuYW1l')
		a1 = a1x.decode('utf-8')

		a2x = base64.b64decode("") # Server URL WebDAV path
		a2 = a2x.decode('utf-8')

		b1x = base64.b64decode("d2ViZGF2X2xvZ2lu")
		b1 = b1x.decode('utf-8')

		b2x = base64.b64decode("") # WebDAV User
		b2 = b2x.decode('utf-8')

		c1x = base64.b64decode("d2ViZGF2X3Bhc3N3b3Jk")
		c1 = c1x.decode('utf-8')

		c2x = base64.b64decode("") # WebDAV Password
		c2 = c2x.decode('utf-8')

		d1x = base64.b64decode("VmVyYm9zZQ==") # Verbosity
		d1 = d1x.decode('utf-8')

		options = {
			a1: a2,
			b1:	b2,
			c1:	c2,
			d1: True,
			'webdav_root': "/"
		}
		return options

	def vars():
		extraparams = {
			'zip7': ("7zG.exe" .format(path)) # 7zG path for extraction + compression
		}

		downparams = { # Download Parameters
			'ZippedRemotePA': (".7z"), # WebDAV path for Zipped file
			'ZippedLocalPA': (".7z" .format(path)), # Local zipped file
			'LocalPA': ("" .format(path)), # Path for unzipped file
		}
		upparams = { # Upload Parameters, same as above
			'ZippedRemotePA': (".7z"),
			'ZippedLocalPA': (".7z" .format(path)),
			'LocalPA': ("" .format(path)),
		}
		cleanupparams = { # Clean-Up parameters for the above files
			'ZippedLocalPA': (".7z" .format(path)),
			'LocalPA': ("" .format(path)),
		}
		return extraparams, downparams, upparams, cleanupparams

	def Download():
		client = Client(SyncBits.CryptSyncVars()) # Declaring WebDAV client
		extraparams, downparams, upparams, cleanupparams = SyncBits.vars() # Takes parameters from SyncBits.vars()

		try:
			PARemoteReady = client.check(downparams['ZippedRemotePA']) # Remote Check
			PALocalReady = os.path.exists(path) # Local Check
			if (PARemoteReady, PALocalReady):
				print("Connected to Server!")
				print("Downloading...")
				try:
					client.download_sync(remote_path=(downparams['ZippedRemotePA']), local_path=(downparams['ZippedLocalPA']))
					print("Resources pulled...")
					if True:
						print("Downloaded Latest Resources from The Server!")
						print("=======================\n")
					else:
						print("No pull!\n")
				except WebDavException as exception:
					print("No pull!\n")

			else:
				print("There was a problem pulling data\n")

		except WebDavException as exception:
			print("Error Code: No Dir \n")
			pass

		print("Extracting resources...") # Once the .7z file is downloaded, it will attempt to extract them and then cleanup.
		try:
			output = '-o'
			extractPAR = extraparams['zip7'], 'x', downparams['ZippedLocalPA'], output+downparams['LocalPA'] # Extracts .7z contents to designated Local path.
			print("Extracting PAR...")
			try:
				subprocess.call(extractPAR) # Extraction
				if True:
					os.remove(downparams['ZippedLocalPA']) # Clean-up
					print("Cleaning up..")
				print("Extracted PAR")
			except ValueError:
				print(ValueError)
			print("=======================\n")
			print("Resources extracted! (Double-check though..)")

		except ValueError:
			print("No Extraction!")

		return

	def Upload():
		client = Client(SyncBits.CryptSyncVars()) # Declaring WebDAV Client
		extraparams, downparams, upparams, cleanupparams = SyncBits.vars() # Takes parameters from SyncBits.vars()
		archivePAR = (extraparams['zip7'], 'a', upparams['ZippedLocalPA'], upparams['LocalPA']) # Preparing Local content for compression and then upload
		try:
			try:
				subprocess.check_output(archivePAR)
				print("Archived PAR")
			except:
				pass
		except:
			print("No archiving!")

		try:
			PARemoteReady = client.check(upparams['ZippedRemotePA']) # Remote check
			PALocalReady = os.path.exists(path) # Local check
			if (PARemoteReady, PALocalReady):
				print("Connected to Server!")
				print("Uploading...")
				try:
					client.upload_sync(remote_path=(upparams['ZippedRemotePA']), local_path=(upparams['ZippedLocalPA'])) # Upload contents
					print("PA Resources pushed...")
					if True:
						print("=======================\n")
						print("Uploaded Latest Resources to The Server!")
						# SyncBits.CleanUp() ## Uncomment for Optional automatic Clean-Up post-upload.
					else:
						print("No push!\n")

				except WebDavException as exception:
					print(exception)
					print("No push!\n")
				return

			else:
				print("There was a problem pushing data\n")

		except:
			print("Error Code: No Dir \n", ValueError)
			pass

		return

	def CleanUp(): # Manual Clean-Up function. Checks directories and REMOVES unzipped local contents + zipped local contents.
		extraparams, downparams, upparams, cleanupparams = SyncBits.vars() # This ASSUMES you have already uploaded the contents to your server safely, and thus can be removed locally.
		LocalResources = [] # Preps list
		LocalResources.append(cleanupparams['LocalPA'])

		ZippedLocalResources = [] # Preps list
		ZippedLocalResources.append(cleanupparams['ZippedLocalPA'])
		print("====================================")
		print("Reached Clean-uP")

		print("Cleaning Directories...")
		for item in LocalResources:
			if os.path.exists(item):
				shutil.rmtree(item) # To be unified for a single dependancy #1
			else:
				print(item, " does not exist.")

		print("Cleaning left-over archives...")
		for item in ZippedLocalResources:
			if os.path.exists(item):
				os.remove(item) # To be unified for a single dependancy #2
			else:
				print(item, " does not exist.")
		try:
			pass

		except:
			print("Clean-Up may not have been successful, do it yourself")
		print("====================================")
		print("Clean-Up done!")
		return


if __name__ == '__main__':
	global path
	path = os.path.dirname(os.path.realpath(sys.argv[0])) # Path becomes the script's path.
	init(convert=True) # Initiating colorama
	os.chdir(path) # Ensuring a CD.
	syscheck()
	while True:
		menu()
