import subprocess

# Scanner for Nextcloud Docker Container. Third script in this collection, can be utilised standalone.

def Scanner():

	try:
		Scan = ['docker', 'exec', 'ContainerName', 'ncp-scan'] # Calls bash within container and triggers ncp-scan
		subprocess.call(Scan)
	except:
		print("Issue calling docker scan")

	try:
		print("Reached second scan!")
		chown = ['chown', 'http:http', '-R', '?/PortableArray/?'] # Update ownership permissions
		chmod = ['chmod', '755', '-R', '?/PortableArray/?'] # Update modding permissions
		subprocess.call(chown)
		print("Chown!")
		subprocess.call(chmod)
		print("Chmod!!")
		return

	except ValueError as exception2:
		print("Issue correcting permissions")
		print(exception2)
		return

if __name__ == '__main__':
	Scanner() # Scanner for Nextcloud docker container
