import subprocess
import time
import socket
import os


class checks():

	def pingtest(Flag):
			target = '192.168.0.1' # Gateway IP
			Servercheck = ("ping -c 5 {}" .format(target))
			try:
					ServerStatus = subprocess.check_output(Servercheck, shell=True) # Attempt to reach Gateway
					Flag = 0
					ServerStatus = True
					return ServerStatus, Flag

			except subprocess.CalledProcessError:
					ServerStatus = False
					Flag = Flag+1 # Incremente Flag
					return ServerStatus, Flag

	def webtest(webFlag):
			s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
			target = 'theServer.URL'
			port = 443

			try:
					s.settimeout(2)
					s.connect((target, port)) # raw socket connect-attempt. If service is responsive, success!
					webFlag = 0
					WebStatus = True
					return WebStatus, webFlag

			except:
					webFlag = webFlag+1
					WebStatus = False
					print("Returning webFlag which incremented to", webFlag)
					return WebStatus, webFlag

	def PATest():
			print("Starting PA Check")
			TriagePath = "?/Triage?" # Triage path

			Resources = []
			PAResource = '.7z'
			Resources.append(PAResource)
			print("Collected PA vars")
			try:
				for item in Resources:
					if os.path.exists("{}/{}" .format(TriagePath, item)): # (TriagePath, .7z item)
						print("exists!")
						PAStatusChecks = True
						return PAStatusChecks
					else:
						PAStatusChecks = False
						continue

			except:
				print("PA excepted!")
				pass
			return PAStatusChecks


class trouble():

	def reboot(): # Reboot server
		reboot = 'reboot'
		subprocess.check_call(reboot, stdin=subprocess.PIPE,  stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
		return

	def rebootweb(): # Restart docker container running WebService
		print("Restarting Docker")
		restart = 'docker restart TheServer'
		subprocess.check_call(restart, stdin=subprocess.PIPE,  stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
		return


def mainServer(Flag):
	ServerStatusCheck, Flag = checks.pingtest(Flag)
	print("Network flag is at", Flag)
	if ServerStatusCheck:
			return
	elif not ServerStatusCheck:
			if Flag > 2: # If gateway cannot be reached more than twice, over a course of x amount of minutes(based on sleep.timer), server will reboot itself. Timer should be set to avoid taking router-reboots as server-issue(aka, give the router enough time to reboot so the server doesn't take it as a local-link issue)
					trouble.reboot()
			else:
					return


def Web(webFlag):
	ServerWebStatusCheck, webFlag = checks.webtest(webFlag)
	print("WebFlag is at", webFlag)
	if ServerWebStatusCheck:
			print("Server up!")
			time.sleep(180)
	elif not ServerWebStatusCheck:
			print(webFlag)
			if webFlag > 1: # If service flag fails more than once, it will be restarted
					print(webFlag)
					print("Restarting...")
					trouble.rebootweb()
			else:
					print("Sleeping... and webFlag @", webFlag)
					time.sleep(180)
	print("returning webflag @", webFlag)
	return webFlag


def PA():
	print("Entered PA")
	PAStatusChecks = checks.PATest() # Checks for new files to sync with server
	print("Finished PA Check")
	if PAStatusChecks:
			print("PA is True!")
			os.system('python ServerDeploymentSERVERhandler.py') # Calls next script if new contents are found in Triage folder
	elif not PAStatusChecks:
			print("PA is False!")
			pass


def counter(Flag, webFlag):
	while True:
			print("webFlag in Counter is at", webFlag)
			Flag = mainServer(Flag) # Server network Check
			webFlag = Web(webFlag) # Web Service Check
			print(webFlag)
			PA() # Files to sync Check
			print(webFlag)


if __name__ == '__main__':
	Flag = 0
	webFlag = 0
	counter(Flag, webFlag)
