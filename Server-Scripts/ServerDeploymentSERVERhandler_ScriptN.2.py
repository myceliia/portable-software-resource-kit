import os
import sys
from ServerScanner import Scanner


def Triage(): # Triaging function

	TriagePath = "?/Triage/?" # Path where new files land
	LatestPath = "?/Latest/?" # Path for up-to-date files to go to
	BackupPath = "?/Backup/?" # Keeping a local backup of previously up-to-date files, just in case

	Resources = []
	PAResource = '.7z'
	Resources.append(PAResource)

	try:
		for item in Resources:
			if os.path.exists("{}/{}" .format(TriagePath, item)):
				try:
					if os.path.exists("{}/{}" .format(LatestPath, item)): # checks if it exists
						os.replace(("{}/{}" .format(LatestPath, item)), ("{}/{}" .format(BackupPath, item))) # Moves previous latest content to backups
						os.replace(("{}/{}" .format(TriagePath, item)), ("{}/{}" .format(LatestPath, item))) # Moves new contents to Latest

					else:
						os.replace(("{}/{}" .format(TriagePath, item)), ("{}/{}" .format(LatestPath, item))) # If no backup is necessary(nothing in Latest), moves triaged to Latest


				except:
					print("Problem is in the recursing Triage!") # Problem in recursive checks
			else:
				print("No {} to Triage!" .format(item))
	except:
		print("There was a problem")
		sys.exit()

	print("Importing Scanner")
	Scanner() # Next script, optional - Mainly for Nextcloud servers
	return

if __name__ == '__main__':
	Triage()
